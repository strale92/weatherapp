import React, { Component } from 'react';
import axios from 'axios';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cityName: '',
      weatherData: null
    };

    this.getCurrentWeather = this.getCurrentWeather.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  getCurrentWeather(event) {
    event.preventDefault();

    const { cityName } = this.state;

    axios
      .get(`http://api.apixu.com/v1/current.json?key=148ee40f66f948f893484500181809&q=${cityName}`)
      // .then(response => console.log(response));
      .then(response => {
        this.setState({ weatherData: response.data });
      });
  }

  handleChange(event) {
    this.setState({ cityName: event.target.value });
  }

  render() {
    const { cityName } = this.state;
    const { weatherData } = this.state;

    let weather;
    if (weatherData) {
      const bgImg = `${weatherData.current.condition.text.toLowerCase()}.jpg`;
      console.log(bgImg);
      weather = (
        <div style={{ backgroundImage: `url(${bgImg})` }}>
          <h1>{weatherData.location.name}</h1>
          <h3>{`${weatherData.current.temp_c} C`}</h3>
          <img
            src={`${weatherData.current.condition.icon}`}
            alt={weatherData.current.condition.text}
          />
        </div>
      );
    } else {
      weather = <h1>nema grada</h1>;
    }
    return (
      <div>
        <form onSubmit={this.getCurrentWeather}>
          <input
            onChange={this.handleChange}
            name="city"
            value={cityName}
            type="text"
            placeholder="Enter city name"
          />
          <input type="submit" value="submit" />
        </form>

        <h1>{weather}</h1>
      </div>
    );
  }
}

export default Home;
